#+TITLE:
#+OPTIONS: \n:t
#+MACRO: inline-image #+HTML:<img src="$1" height="18" style="display:inline;"/>

Repositories:

- [[https://bitbucket.org/stakahama/aprlspec][APRLspec]]: Spectra object definitions
- [[https://bitbucket.org/stakahama/aprlssb][APRLssb]]: Smoothing-Splines Baseline
- [[https://bitbucket.org/stakahama/aprlmpf][APRLmpf]]: Multipeak Fitting
- [[https://bitbucket.org/stakahama/aprlmvr][APRLmvr]]: Multivariate Regression
- [[https://bitbucket.org/stakahama/aprl-kpp-gp][APRL-KPP-GP]]: G/P module addition to KPP

#+BEGIN_HTML
<table>
<tr>
<td> I can be reached at </td>
<td>
<img src="./personnel/Email_st.png" height="20" style="display:inline;float:$1;margin:0px 0px 0px 5px;"/>
</td>
</tr>
</table>
#+END_HTML
